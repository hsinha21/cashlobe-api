const mongoose = require('mongoose');

let model = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    balanceSheetData: {
        type: Object
    }
})


let BalanceSheet = mongoose.model('BalanceSheet', model);

module.exports = BalanceSheet;