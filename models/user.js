const mongoose = require('mongoose');

let model = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    organization: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    onboardedDate: {
        type: Date,
        require: true
    },
    xeroTokens: {
        type: Object,
    }
})


let User = mongoose.model('User', model);

module.exports = User;