const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require('path');
const mongoose = require('mongoose');

const port = process.env.PORT || 4000;
const app = express();

const xeroRoutes = require('./routes/xeroRoutes')

app.use(cors(), bodyParser.json(), bodyParser.urlencoded({ extended: false }));


app.use(express.static(__dirname + '/public'));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/index.html'))
});


app.use('/apis', xeroRoutes)

const dbConnection = 'mongodb+srv://cashlobedb:cashlobetest123@cluster0.kp4qw.mongodb.net/cashdb?retryWrites=true&w=majority';
mongoose.connect(dbConnection, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (err) => {
    if (err) console.log(err);
    else {
        console.log("Database Connected");
    }
});


app.listen(port, () => {
    console.log("server running on :" + port);
});

