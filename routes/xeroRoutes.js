const express = require("express");
const routes = express.Router();
const request = require('request');
const mongoose = require('mongoose');
const User = require("../models/user");
const mailingConfig = require('../config/mailConfig');
const BalanceSheet = require("../models/balanceSheet");

let transporter = mailingConfig.transporter;
const clientId = '8090A29A30E14C6DA18F23BE33770316'
const clientSecret = 'SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ'

routes.post('/addNewUser', (req, res) => {
    // console.log('addNewUser', req.body);
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        organization: req.body.org,
        role: req.body.role,
        onboardedDate: new Date()
    })

    newUser.save((error, savedUser) => {
        if (error) {
            res.json({ success: false, error: error })
        } else {
            console.log(savedUser);
            let newBS = new BalanceSheet({ userId: savedUser._id }).save();

            res.json({ success: true, msg: 'User Added to app' })

        }
    })
})

routes.get('/getAllUsers', (req, res) => {
    // console.log('getAllUsers');
    User.find((error, allUsers) => {
        if (error) {
            res.json({ success: false, error: error })
        } else {
            res.json({ success: true, msg: 'All User fetched', data: allUsers })

        }
    })
})

routes.post('/sendConsentEmail', (req, res) => {
    console.log('sendConsentEmail', req.body);

    let mailOptions = {
        to: req.body.email,
        subject: "Cashlobe Xero Access Concent",
        html:
            `Hi ${req.body.name}, <br><br>
            Cashlobe would like to access your Xero account data.<br><br>
            Click <a href="http://localhost:4200">here</a> to prodive your consent.<br><br>
            Regards,<br>
            Cashlobe Support Team
            `
    }

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.error(error)
            res.json({ success: false, error: error })
        } else {
            console.log('Consent Email Sent')
            console.log(info.response)
            res.json({ success: true, msg: 'Consent Email Sent' })

        }
    })
})

routes.post('/addXeroDataOfUser', (req, res) => {
    // console.log('addXeroDataOfUser', req.body);
    User.updateOne({ email: req.body.xeroUserData.email }, {
        xeroTokens: { accessToken: req.body.accessToken, refreshToken: req.body.refreshToken }
    }, (error, userUpdated) => {
        if (error) {
            console.error(error)
            res.json({ success: false, error: error })
        } else {
            res.json({ success: true, msg: 'User Token Added' })
        }
    })
})

routes.get('/getUserTenents/:userId', (req, res) => {
    console.log('getUserTenents', req.params.userId);

    User.findById(req.params.userId, (error, userData) => {
        if (error) {
            console.log(error);
        } else {
            // console.log(userData);

            refreshAccessToken(userData.xeroTokens.refreshToken, (error, tokenData) => {
                let headers = {
                    'Authorization': 'Bearer ' + tokenData.access_token
                };

                request.get({
                    'headers': headers,
                    'url': `https://api.xero.com/connections`,
                    'observe': 'response'
                }, (error, response, body) => {
                    if (error) {
                        console.log(error);
                    } else {
                        let resData2 = JSON.parse(body)
                        // console.log(resData2);

                        if (resData2) {
                            res.json({ success: true, data: resData2 })
                        } else {
                            res.json({ success: false, error: resData2 })
                        }
                    }
                })
            })

        }
    })
})

routes.post('/getUserBalanceSheetReport', (req, res) => {
    console.log('getUserBalanceSheetReport', req.body);

    User.findById(req.body.userId, (error, userData) => {
        if (error) {
            console.log(error);
        } else {
            // console.log(userData);
            refreshAccessToken(userData.xeroTokens.refreshToken, (error, tokenData) => {

                let headers = {
                    'Authorization': 'Bearer ' + tokenData.access_token,
                    'xero-tenant-id': req.body.tenantID,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                };

                request.get({
                    'headers': headers,
                    'url': `https://api.xero.com/api.xro/2.0/Reports/BalanceSheet`,
                    'qs': { timeframe: req.body.timeFrame ? req.body.timeFrame : '', periods: req.body.period ? req.body.period : '' },
                    'observe': 'response'
                }, (error, response, body) => {
                    if (error) {
                        console.log(error);
                    } else {
                        let resData2 = JSON.parse(body)
                        // console.log(resData2);

                        if (resData2) {

                            BalanceSheet.updateOne({ userId: req.body.userId }, { userId: req.body.userId, balanceSheetData: resData2.Reports[0] }, (error, bsUpdated) => {
                                if (error) console.log(error);
                                else console.log('Balance Sheet Updated');
                            })

                            res.json({ success: true, data: resData2 })
                        } else {
                            res.json({ success: false, error: resData2 })
                        }
                    }
                })
            })
        }
    })

})

routes.get('/xeroTenantConnections/:token', (req, res) => {

    refreshAccessToken(req.params.token, (error, tokenData) => {
        let headers = {
            'Authorization': 'Bearer ' + tokenData.access_token
        };

        request.get({
            'headers': headers,
            'url': `https://api.xero.com/connections`,
            'observe': 'response'
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                let resData2 = JSON.parse(body)
                // console.log(resData2);

                if (resData2) {
                    res.json({ success: true, data: resData2 })
                } else {
                    res.json({ success: false, error: resData2 })
                }
            }
        })
    })
})

routes.post('/xeroBalanceSheetReport', (req, res) => {

    refreshAccessToken(req.body.token, (error, tokenData) => {

        let headers = {
            'Authorization': 'Bearer ' + tokenData.access_token,
            'xero-tenant-id': '62b0e73a-bbe7-4451-ae21-4b24ec8e1189',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        request.get({
            'headers': headers,
            'url': `https://api.xero.com/api.xro/2.0/Reports/BalanceSheet`,
            'qs': { timeframe: req.body.timeFrame ? req.body.timeFrame : '', periods: req.body.period ? req.body.period : '' },
            'observe': 'response'
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                let resData2 = JSON.parse(body)
                console.log(resData2);

                if (resData2) {

                    res.json({ success: true, data: resData2 })
                } else {
                    res.json({ success: false, error: resData2 })
                }
            }
        })
    })

})

// routes.get('/refreshXeroToken/:refreshToken', (req, res) => {
//     console.log('refreshXeroToken..........', req.params);

let refreshAccessToken = (refreshToken, callback) => {

    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'grant_type': 'refresh_token',
    };

    let body = new URLSearchParams();
    body.set('grant_type', 'refresh_token')
    body.set('refresh_token', refreshToken)
    body.set('client_id', clientId)
    body.set('client_secret', clientSecret)

    // console.log(body);

    request.post({
        'headers': headers,
        'url': `https://identity.xero.com/connect/token`,
        'body': body.toString(),
        'observe': 'response'
    }, (error, response, body) => {
        if (error) {
            console.log(error);
        } else {
            let resData2 = JSON.parse(body)
            // console.log(resData2);

            callback(null, resData2)

            // if (resData2) {
            //     res.json({ success: true, data: resData2 })
            // } else {
            //     res.json({ success: false, error: resData2 })
            // }
        }
    })
}
// })


module.exports = routes;